MATLAB code accompanying the manuscript "Emergent rheotaxis of shape-changing swimmers in Poiseuille flow" by B. J. Walker, K. Ishimoto, C. Moreau, E. A. Gaffney, and M. P. Dalwadi, available at https://doi.org/10.1017/jfm.2022.474.

------
Usage
------
Parameters and setup should be set in init.m, following the notation of the accompanying manuscript. Various comparisons can be run and plots can be generated, with init.m being run before each new exploration or plot.

------
License
------
This source code is licensed under a Creative Commons Attribution 4.0 International License (https://creativecommons.org/licenses/by/4.0).
